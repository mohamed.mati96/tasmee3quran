//
//  UserDefault.swift
//  RecitationQuran
//
//  Created by maati on 9/9/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation

struct UserDefault {
    
    static var instance = UserDefault()
       // User Defaults
     let defaults = UserDefaults.standard
       var isLoggedIn: Bool {
           get {
               return defaults.bool(forKey: "isLoggedIn")
           }
           set {
               defaults.set(newValue, forKey: "isLoggedIn")
           }
       }
       
       var authToken: String {
           get {
               
            return (defaults.string(forKey: "api_token")) ?? ""
           }
           set {
               defaults.set(newValue, forKey: "api_token")
               defaults.synchronize()

           }
       }
       
    
    var userId: String {
        get {
            
         return (defaults.string(forKey: "user_id")) ?? ""
        }
        set {
            defaults.set(newValue, forKey: "user_id")
            defaults.synchronize()

        }
    }
       var name: String {
           get {
             return defaults.value(forKey: "name") as! String
           }
           set {
               defaults.set(newValue, forKey: "name")
               defaults.synchronize()
           }
         
       }
}
