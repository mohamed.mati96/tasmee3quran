//
//  Data.swift
//  RecitationQuran
//
//  Created by maati on 9/10/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation
extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
