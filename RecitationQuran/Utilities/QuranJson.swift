//
//  QuranJson.swift
//  RecitationQuran
//
//  Created by maati on 9/12/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation

public var QuranJson = """
[ {
    "sourahIndex" : 1,
    "startPage": 1,
    "endPage" : 1
},
{
     "sourahIndex" : 2,
       "startPage": 2,
       "endPage" : 49
},
{
     "sourahIndex" : 3,
       "startPage": 50,
       "endPage" : 76
},
{
     "sourahIndex" : 4,
       "startPage": 77,
       "endPage" : 106
},
{
     "sourahIndex" : 5,
       "startPage": 106,
       "endPage" : 127
},
{
     "sourahIndex" : 6,
       "startPage": 128,
       "endPage" : 150
},
{
     "sourahIndex" : 7,
       "startPage": 151,
       "endPage" : 176
},
{
     "sourahIndex" : 8,
       "startPage": 177,
       "endPage" : 186
},
{
     "sourahIndex" : 9,
       "startPage": 187,
       "endPage" : 207
},
{
     "sourahIndex" : 10,
       "startPage": 208,
       "endPage" : 221
},
{
     "sourahIndex" : 11,
       "startPage": 221,
       "endPage" : 235
},
{
     "sourahIndex" : 12,
       "startPage": 235,
       "endPage" : 248
},
{
     "sourahIndex" : 13,
       "startPage": 249,
       "endPage" : 255
},
{
     "sourahIndex" : 14,
       "startPage": 255,
       "endPage" : 261
},
{
     "sourahIndex" : 15,
       "startPage": 262,
       "endPage" : 267
},
{
     "sourahIndex" : 16,
       "startPage": 267,
       "endPage" : 281
},
{
     "sourahIndex" : 17,
       "startPage": 282,
       "endPage" : 293
},
{
     "sourahIndex" : 18,
       "startPage": 293,
       "endPage" : 304
},
{
     "sourahIndex" : 19,
       "startPage": 305,
       "endPage" : 312
},
{
     "sourahIndex" : 20,
       "startPage": 312,
       "endPage" : 321
},
{
     "sourahIndex" : 21,
       "startPage": 322,
       "endPage" : 331
},
{
     "sourahIndex" : 22,
       "startPage": 332,
       "endPage" : 341
},
{
     "sourahIndex" : 23,
       "startPage": 342,
       "endPage" : 349
},
{
     "sourahIndex" : 24,
       "startPage": 350,
       "endPage" : 359
},
{
     "sourahIndex" : 25,
       "startPage": 359,
       "endPage" : 366
},
{
     "sourahIndex" : 26,
       "startPage": 367,
       "endPage" : 376
},
{
     "sourahIndex" : 27,
       "startPage": 377,
       "endPage" : 285
},
{
     "sourahIndex" : 28,
       "startPage": 396,
       "endPage" : 404
},
{
     "sourahIndex" : 29,
       "startPage": 404,
       "endPage" : 410
},
{
     "sourahIndex" : 30,
       "startPage": 411,
       "endPage" : 414
},
{
     "sourahIndex" : 31,
       "startPage": 415,
       "endPage" : 417
},
{
     "sourahIndex" : 32,
       "startPage": 418,
       "endPage" : 427
},
{
     "sourahIndex" : 33,
       "startPage": 428,
       "endPage" : 434
},
{
     "sourahIndex" : 34,
       "startPage": 434,
       "endPage" : 440
},
{
     "sourahIndex" : 35,
       "startPage": 440,
       "endPage" : 445
},
{
     "sourahIndex" : 36,
       "startPage": 446,
       "endPage" : 452
},
{
     "sourahIndex" : 37,
       "startPage": 453,
       "endPage" : 458
},
{
     "sourahIndex" : 38,
       "startPage": 458,
       "endPage" : 467
},
{
     "sourahIndex" : 39,
       "startPage": 467,
       "endPage" : 476
},
{
     "sourahIndex" : 40,
       "startPage": 477,
       "endPage" : 482
},
{
     "sourahIndex" : 41,
       "startPage": 483,
       "endPage" : 489
},
{
     "sourahIndex" : 42,
       "startPage": 489,
       "endPage" : 495
},
{
     "sourahIndex" : 43,
       "startPage": 496,
       "endPage" : 498
},
{
     "sourahIndex" : 44,
       "startPage": 499,
       "endPage" : 502
},
{
     "sourahIndex" : 45,
       "startPage": 502,
       "endPage" : 506
},
{
     "sourahIndex" : 46,
       "startPage": 507,
       "endPage" : 510
},
{
     "sourahIndex" : 47,
       "startPage": 511,
       "endPage" : 515
},
{
     "sourahIndex" : 48,
       "startPage": 515,
       "endPage" : 517
},
{
     "sourahIndex" : 49,
       "startPage": 518,
       "endPage" : 520
},
{
     "sourahIndex" : 50,
       "startPage": 520,
       "endPage" : 523
},
{
     "sourahIndex" : 51,
       "startPage": 523,
       "endPage" : 525
},
{
     "sourahIndex" : 52,
       "startPage": 526,
       "endPage" : 528
},
{
     "sourahIndex" : 53,
       "startPage": 528,
       "endPage" : 531
},
{
     "sourahIndex" : 54,
       "startPage": 531,
       "endPage" : 534
},
{
     "sourahIndex" : 55,
       "startPage": 534,
       "endPage" : 537
},
{
     "sourahIndex" : 56,
       "startPage": 537,
       "endPage" : 541
},
{
     "sourahIndex" : 57,
       "startPage": 542,
       "endPage" : 545
},
{
     "sourahIndex" : 58,
       "startPage": 545,
       "endPage" : 548
},
{
     "sourahIndex" : 59,
       "startPage": 549,
       "endPage" : 551
},
{
     "sourahIndex" : 60,
       "startPage": 551,
       "endPage" : 552
},
{
     "sourahIndex" : 61,
       "startPage": 553,
       "endPage" : 554
},
{
     "sourahIndex" : 62,
       "startPage": 554,
       "endPage" : 555
},
{
     "sourahIndex" : 63,
       "startPage": 556,
       "endPage" : 557
},
{
     "sourahIndex" : 64,
       "startPage": 558,
       "endPage" : 559
},
{
     "sourahIndex" : 65,
       "startPage": 560 ,
       "endPage" : 561
},
{
     "sourahIndex" : 66,
       "startPage": 562,
       "endPage" : 564
},
{
     "sourahIndex" : 67,
       "startPage": 564,
       "endPage" : 566
},
{
     "sourahIndex" : 68,
       "startPage": 566,
       "endPage" : 568
},
{
     "sourahIndex" : 69,
       "startPage": 568,
       "endPage" : 570
},
{
     "sourahIndex" : 70,
       "startPage": 570,
       "endPage" : 571
},
{
     "sourahIndex" : 71,
       "startPage": 572,
       "endPage" : 573
},
{
     "sourahIndex" : 72,
       "startPage": 574,
       "endPage" : 575
},
{
     "sourahIndex" : 73,
       "startPage": 575,
       "endPage" : 577
},
{
     "sourahIndex" : 74,
       "startPage": 577,
       "endPage" : 578
},
{
     "sourahIndex" : 75,
       "startPage": 578 ,
       "endPage" : 580
},
{
     "sourahIndex" : 76,
       "startPage": 580,
       "endPage" : 581
},
{
     "sourahIndex" : 77,
       "startPage": 582,
       "endPage" : 583
},
{
     "sourahIndex" : 78,
       "startPage": 583,
       "endPage" : 584
},
{
     "sourahIndex" : 79,
       "startPage": 585,
       "endPage" : 586
},
{
     "sourahIndex" : 80,
       "startPage": 586,
       "endPage" : 586
},
{
     "sourahIndex" : 81,
       "startPage": 587,
       "endPage" : 587
},
{
     "sourahIndex" : 82,
       "startPage": 587,
       "endPage" : 589
},
{
     "sourahIndex" : 83,
       "startPage": 589 ,
       "endPage" : 590
},
{
     "sourahIndex" : 84,
       "startPage": 590,
       "endPage" : 590
},
{
     "sourahIndex" : 85,
       "startPage": 591,
       "endPage" : 591
},
{
     "sourahIndex" : 86,
       "startPage": 591,
       "endPage" : 592
},
{
     "sourahIndex" : 87,
       "startPage": 592,
       "endPage" : 593
},
{
     "sourahIndex" : 88,
       "startPage": 593,
       "endPage" : 594
},
{
     "sourahIndex" : 89,
       "startPage": 594,
       "endPage" : 595
},
{
     "sourahIndex" : 90,
       "startPage": 595,
       "endPage" : 595
},
{
     "sourahIndex" : 91,
       "startPage": 595,
       "endPage" : 596
},
{
     "sourahIndex" : 92,
       "startPage": 596,
       "endPage" : 597
},
{
     "sourahIndex" : 93,
       "startPage": 597,
       "endPage" : 597
},
{
     "sourahIndex" : 94,
       "startPage": 597,
       "endPage" : 597
},
{
     "sourahIndex" : 95,
       "startPage": 598,
       "endPage" : 598
},
{
     "sourahIndex" : 96,
       "startPage": 598,
       "endPage" : 598
},
{
     "sourahIndex" : 97,
       "startPage": 598,
       "endPage" : 598
},
{
     "sourahIndex" : 98,
       "startPage": 599,
       "endPage" : 599
},
{
     "sourahIndex" : 99,
       "startPage": 599,
       "endPage" : 599
},
{
     "sourahIndex" : 100,
       "startPage": 599,
       "endPage" : 600
},
{
     "sourahIndex" : 101,
       "startPage": 600,
       "endPage" : 600
},
{
     "sourahIndex" : 102,
       "startPage": 600,
       "endPage" : 600
}
,
{
     "sourahIndex" : 103,
       "startPage": 601,
       "endPage" : 601
},
{
     "sourahIndex" : 104,
       "startPage": 601,
       "endPage" : 601
},
{
     "sourahIndex" : 105,
       "startPage": 601,
       "endPage" : 601
},
{
     "sourahIndex" : 106,
       "startPage": 601,
       "endPage" : 601
},
{
     "sourahIndex" : 107,
       "startPage": 602,
       "endPage" : 602
},
{
     "sourahIndex" : 108,
       "startPage": 602,
       "endPage" : 602
},
{
     "sourahIndex" : 109,
       "startPage": 603,
       "endPage" : 603
},
{
     "sourahIndex" : 110,
       "startPage": 603,
       "endPage" : 603
},
{
     "sourahIndex" : 111,
       "startPage": 603,
       "endPage" : 603
},
{
     "sourahIndex" : 112,
       "startPage": 604,
       "endPage" : 604
},
{
     "sourahIndex" : 113,
       "startPage": 604,
       "endPage" : 604
},
{
     "sourahIndex" : 114,
       "startPage": 604,
       "endPage" : 604
}







]
"""
