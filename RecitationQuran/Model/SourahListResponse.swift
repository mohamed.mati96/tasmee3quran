//
//  SourahListResponse.swift
//  RecitationQuran
//
//  Created by maati on 9/10/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation


struct SourahListRepsonse : Codable {

        let code : Int?
        let data : [SourahList]?
        let status : String?

        enum CodingKeys: String, CodingKey {
                case code = "code"
                case data = "data"
                case status = "status"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                code = try values.decodeIfPresent(Int.self, forKey: .code)
                data = try values.decodeIfPresent([SourahList].self, forKey: .data)
                status = try values.decodeIfPresent(String.self, forKey: .status)
        }

}


struct SourahList : Codable {

    
        let name : String?
        let number : Int?
        let numberOfAyahs : Int?
        let revelationType : String?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case number = "number"
                case numberOfAyahs = "numberOfAyahs"
                case revelationType = "revelationType"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                name = try values.decodeIfPresent(String.self, forKey: .name)
                number = try values.decodeIfPresent(Int.self, forKey: .number)
                numberOfAyahs = try values.decodeIfPresent(Int.self, forKey: .numberOfAyahs)
                revelationType = try values.decodeIfPresent(String.self, forKey: .revelationType)
        }

}
