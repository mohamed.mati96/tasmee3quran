//
//  TeacherProfileResonse.swift
//  RecitationQuran
//
//  Created by maati on 9/11/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation
// MARK: - TeacherProfileResonse
struct TeacherProfileResonse: Codable {
    let status: Int?
    let message: String?
    let data: TeacherProfile?
}

// MARK: - DataClass
struct TeacherProfile: Codable {
    let id: Int?
    let name, email, image, phone: String?
    let instagramLink, twitterLink: String?
    let gender, createdAt, updatedAt: String?
    let imageFullPath: String?

    enum CodingKeys: String, CodingKey {
        case id, name, email, image, phone
        case instagramLink = "instagram_link"
        case twitterLink = "twitter_link"
        case gender
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case imageFullPath = "image_full_path"
    }
}
