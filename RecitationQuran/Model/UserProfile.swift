//
//  UserProfile.swift
//  RecitationQuran
//
//  Created by maati on 9/23/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation

// MARK: - UserProfile
struct UserProfile: Codable {
    let status: Int?
    let message: String?
    let data: Profile?
}

// MARK: - DataClass
struct Profile: Codable {
    var id: Int?
    let createdAt, updatedAt, name, email: String?
    let gender: String?

    enum CodingKeys: String, CodingKey {
        case id
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case name, email, gender
    }
}
