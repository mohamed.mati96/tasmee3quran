//
//  Login.swift
//  RecitationQuran
//
//  Created by maati on 9/9/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation
// MARK: - LoginUser
struct LoginUser: Codable {
    let status: Int?
    let message: String?
    let data: LoginInfo?
}

// MARK: - DataClass
struct LoginInfo: Codable {
    let apiToken: String?
    let client: Client?

    enum CodingKeys: String, CodingKey {
        case apiToken = "api_token"
        case client
    }
}

// MARK: - Client
struct Client: Codable {
    let id: Int?
    let createdAt, updatedAt, name, email: String?
    let gender: String?

    enum CodingKeys: String, CodingKey {
        case id
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case name, email, gender
    }
}
