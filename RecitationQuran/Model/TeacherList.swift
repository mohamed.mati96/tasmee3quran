//
//  TeacherList.swift
//  RecitationQuran
//
//  Created by maati on 9/9/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation

// MARK: - TeachersListRepsonse
struct TeachersListRepsonse: Codable {
    let status: Int?
    let message: String?
    let data: [Teachers]?
}

// MARK: - Datum
struct Teachers: Codable {
    let id: Int?
    let name, image: String?
    let imageFullPath: String?
   

    enum CodingKeys: String, CodingKey {
        case id, name, image
        case imageFullPath = "image_full_path"
    }
}
