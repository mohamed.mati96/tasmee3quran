//
//  ResetPass.swift
//  RecitationQuran
//
//  Created by maati on 9/20/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation
// MARK: - ResetPass
struct ResetPass: Codable {
    let status: Int
    let message: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let pinCodeForTest: Int

    enum CodingKeys: String, CodingKey {
        case pinCodeForTest = "pin code for test"
    }
}



// MARK: - NewPassword
struct NewPassword: Codable {
    let status: Int
    let message, data: String?
}
