//
//  Register.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 9/7/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation
// MARK: - RegisterContainer
struct RegisterContainer: Codable {
    let status: Int?
    let message: String?
    let data: RegisterData?
}

// MARK: - RegisterData
struct RegisterData: Codable {
    let apiToken: String?
    let client: RegistInfo?

    enum CodingKeys: String, CodingKey {
        case apiToken = "api_token"
        case client
    }
}

// MARK: - RegistInfo
struct RegistInfo: Codable {
    let name, email, gender, updatedAt: String?
    let createdAt: String?
    let id: Int?

    enum CodingKeys: String, CodingKey {
        case name, email, gender
        case updatedAt = "updated_at"
        case createdAt = "created_at"
        case id
    }
}
