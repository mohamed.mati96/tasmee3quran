//
//  SourahDetail.swift
//  RecitationQuran
//
//  Created by maati on 9/10/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation

struct Sourah: Codable {
    let sourahIndex: Int?
    let startPage: Int?
    let endPage: Int?
}
