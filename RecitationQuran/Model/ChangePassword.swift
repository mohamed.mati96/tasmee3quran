//
//  ChangePassword.swift
//  RecitationQuran
//
//  Created by maati on 9/28/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation

// MARK: - ChangePassword
struct ChangePassword: Codable {
    let status: Int?
    let message: String?
    let data: Password?
}

// MARK: - DataClass
struct Password: Codable {
    let password: [String]?
}
