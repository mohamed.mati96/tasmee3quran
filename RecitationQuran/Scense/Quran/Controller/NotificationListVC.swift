//
//  NotificationListVC.swift
//  RecitationQuran
//
//  Created by maati on 10/4/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit

class NotificationListVC: UITableViewController {
   private let identifier = "notificationCell"
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "الاشعارات"
       configureTable()

    }

    fileprivate func configureTable() {
        tableView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: identifier)
        tableView.rowHeight = 90
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? NotificationCell
        return cell!
    }
    


}
