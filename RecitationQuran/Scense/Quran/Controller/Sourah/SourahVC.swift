//
//  SourahVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/29/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class SourahVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var menu: UISideMenuNavigationController?
    var quranName: SourahList?
    var sourahNumber = 1
    var pages = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollection()
        navigationController?.navigationItem.title = quranName?.name
    }
   
    private func configureCollection() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }

}

extension SourahVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SourahCell", for: indexPath) as! SourahCell
        cell.configure(imageIndex: pages[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
  
   
}
