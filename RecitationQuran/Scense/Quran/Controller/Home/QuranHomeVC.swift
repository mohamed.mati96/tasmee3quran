//
//  QuranHomeVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/29/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Alamofire
import SideMenu


class QuranHomeVC: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    var quran =  [SourahList]()  { didSet { filteredQuran = quran  } }
    var filteredQuran  = [SourahList]() { didSet { collectionView.reloadData() }}
    private var sideMenu: UISideMenuNavigationController?
    var profile = ProfileVC()
    let reviewService = ReviewService.shared
    private let search = UISearchController(searchResultsController: nil)
    var isSearchBarEmpty: Bool {
        return search.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return search.isActive && !isSearchBarEmpty
    }
    var userId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "القرآن الكريم"
        searchBar.delegate = self
        configureCollection()
        configreMenu()
        //        addChildControllers()
        configreNavigation()
        configureSearch()
        featchData()
        
    }
    
    private func configreNavigation() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "text.justify"), style: .plain, target: self, action: #selector(menuTapped))
        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
        let textAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1), NSAttributedString.Key.font: UIFont(name: "Tajawal-Medium", size: 20) ?? 16] as [NSAttributedString.Key : Any]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "bell"), style: .plain, target: self, action: #selector(notificationListTapped))
    }
    
    private func configureSearch() {
        search.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        
    }
    @objc func notificationListTapped() {
        let notificationVC = NotificationListVC()
        navigationController?.pushViewController(notificationVC, animated: true)
    }
    @objc func menuTapped() {
        present(sideMenu!, animated: true)
    }
    
    
    
    public  func rateApp() {
        let deadline = DispatchTime.now() + .seconds(2)
        DispatchQueue.main.asyncAfter(deadline: deadline) { [weak self] in
            self?.reviewService.requestReview(isWrittenReview: true)
        }
    }
    
    func shareApp() {
        if let name = URL(string: "https://itunes.apple.com/us/app/myapp/idxxxxxxxx?ls=1&mt=8"), !name.absoluteString.isEmpty {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
            if self.presentedViewController != nil {
                self.dismiss(animated: false, completion: {
                    [unowned self] in
                    self.present(activityVC, animated: true, completion: nil)
                })
            }else{
                self.present(activityVC, animated: true, completion: nil)
            }
        }else  {
            // show alert for not available
        }
    }
    func featchData()  {
        view.showActivityIndicator(isUserInteractionEnabled: true)
        Alamofire.request(SourahRouter.sourahList).debugLog().responseData { [weak self] response in
            guard let self = self else { return }
            self.view.hideActivityIndicator()
            switch response.result {
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(SourahListRepsonse.self, from: data)
                    self.quran = result.data ?? []
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    func getSourah(at index: Int) -> Sourah {
        let jsonData = QuranJson.data(using: .utf8)!
        let result = try! JSONDecoder().decode([Sourah].self, from:jsonData)
        return result.first(where: {$0.sourahIndex == index })!
    }
    
}


extension QuranHomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollection() {
        collectionView.register(UINib(nibName: "QuranHomeCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.semanticContentAttribute = .forceRightToLeft
        let width = (view.frame.size.width - 10) / 3
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredQuran.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuranHomeCell
        cell.configre(item: filteredQuran[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let tabBarConttroller = self.storyboard?.instantiateViewController(identifier: "tabController") as! UITabBarController
        tabBarConttroller.selectedIndex = 0
        if let sourahViewController = tabBarConttroller.selectedViewController as? SourahVC {
            let sourah = getSourah(at: indexPath.item + 1)
            sourahViewController.pages = Array((sourah.startPage ?? 0)...(sourah.endPage ?? 0))
            sourahViewController.title = filteredQuran[indexPath.item].name
//            sourahViewController.navigationController?.navigationItem.title = filteredQuran[indexPath.item].name
        }
        self.navigationController?.pushViewController(tabBarConttroller, animated: true)
    }
}

extension QuranHomeVC: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredQuran = searchText.isEmpty ?  quran : quran.filter { $0.name!.lowercased().contains(searchText.lowercased())}
    }
}


extension QuranHomeVC: MenuControllerDelegate {
    
    func configreMenu() {
        let menu = SideMenuVC(with: SideMenuItem.allCases)
        menu.delegate = self
        sideMenu?.leftSide = true
        sideMenu = UISideMenuNavigationController(rootViewController: menu)
        SideMenuManager.default.menuLeftNavigationController = sideMenu
        SideMenuManager.default.menuAddPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: view)
    }
    
    func didSelectMenuItem(named: SideMenuItem) {
        switch named {
        case .profile:
            profile.view.isHidden = false
            guard let profileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "profile") as? ProfileVC else { return }
            self.dismiss(animated: true) {
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
        case .home:
            profile.view.isHidden = true
            
        case .shareApp:
            profile.view.isHidden = true
            shareApp()
        case .rateApp:
            profile.view.isHidden = true
            rateApp()
        case .logout:
            profile.view.isHidden = true
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let login = storyboard.instantiateViewController(identifier: "login") as? LoginVC else { return }
                let nav = UINavigationController(rootViewController: login)
                nav.navigationBar.isHidden = true
                nav.modalPresentationStyle = .overFullScreen
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                self.navigationController?.navigationBar.shadowImage = UIImage()
                self.navigationController?.navigationBar.isTranslucent = true
                self.navigationController?.view.backgroundColor = .clear
                self.navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
                let textAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1), NSAttributedString.Key.font: UIFont(name: "Tajawal-Medium", size: 20) ?? 16] as [NSAttributedString.Key : Any]
                self.navigationController?.navigationBar.titleTextAttributes = textAttributes
                UserDefault.instance.isLoggedIn = false
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                self.dismiss(animated: true) {
                    self.present(nav, animated: true, completion: nil)
                }
            }
        }
    }
    
    
}

