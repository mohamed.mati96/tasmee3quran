//
//  SourahCell.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/30/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Kingfisher
class SourahCell: UICollectionViewCell {
    
 
    @IBOutlet weak var sourahImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configure(imageIndex: Int) {
        let imageStringUrl = "http://islamicbook.ws/1/\(imageIndex).png"
        guard let imageUrl = URL(string: imageStringUrl) else { return }
        sourahImageView.kf.indicatorType = .activity
        sourahImageView.kf.setImage(with: imageUrl)
    }
    
}
