//
//  QuranHomeCell.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/29/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit

class QuranHomeCell: UICollectionViewCell {

    @IBOutlet weak var sourahTtileLabel: UILabel!
    @IBOutlet weak var quranImageView: UIImageView! {
        didSet {
            quranImageView.layer.cornerRadius = quranImageView.frame.size.width / 2
            quranImageView.clipsToBounds = true
            quranImageView.image = UIImage(named: "quran")
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configre(item: SourahList) {
        sourahTtileLabel.text = item.name
        
    }
}
