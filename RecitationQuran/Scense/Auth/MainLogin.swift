//
//  ViewController.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/27/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit

final class MainLogin: UIViewController {
    
    @IBOutlet private weak var createNewAccountBtn: UIButton! {
        didSet {
            createNewAccountBtn.layer.cornerRadius = 10
        }
    }
    @IBOutlet private weak var loginBtn: UIButton! {
        didSet {
            loginBtn.layer.cornerRadius = 10
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    
    @IBAction fileprivate func loginPressed(_ sender: UIButton) {
        let login = (self.storyboard?.instantiateViewController(withIdentifier: "login"))! as! LoginVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction fileprivate func createNewAccountPressed(_ sender: UIButton) {
        let register = (self.storyboard?.instantiateViewController(withIdentifier: "register"))! as! RegisterVC
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    
}

