//
//  ForgetPassword.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/28/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Alamofire

final class ForgetPasswordVC: UIViewController {
    
    @IBOutlet private weak var containueButton: UIButton! {
        didSet {
            containueButton.layer.cornerRadius = 10
            containueButton.clipsToBounds = true
        }
    }
    
    @IBOutlet private weak var forgetPassTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigation()
    }
    
    fileprivate func configureNavigation() {
        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
        let textAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1), NSAttributedString.Key.font: UIFont(name: "Tajawal-Medium", size: 20) ?? 16] as [NSAttributedString.Key : Any]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    @IBAction func continueButtonTapped(_ sender: UIButton) {
       validate()
    }
    
    func forgetPass(email: String) {
        Alamofire.request(AuthRouter.forgetPass(email: email)).responseData { [weak self ] (response) in
            //            print("Show Response: ",String.init(data:response.data!, encoding:.utf8)!)
            guard let self = self else { return }
            switch response.result {
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(ResetPass.self, from: data)
                    print(result)
                    switch result.status {
                    case 1:
                        let  newPass = (self.storyboard?.instantiateViewController(withIdentifier: "CreateNewPassword"))! as! CreateNewPasswordVC
                        self.navigationController?.pushViewController(newPass, animated: true)
                    case 0:
                        self.showAlert(for: "ادخل البريد الالكتروني بشكل صحيح")
                    default:
                        break
                    }
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
        
    }
    
    func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "متابعة", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func validate() {
        do {
            let email = try forgetPassTF.validatedText(validationType: ValidatorType.email)
            forgetPass(email: email)
            
        } catch(let error) {
            showAlert(for: (error as! ValidationError).message)
        }
    }
    
}
