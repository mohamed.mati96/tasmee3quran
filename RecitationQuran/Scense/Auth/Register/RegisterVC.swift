//
//  RegisterVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/27/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Alamofire

class RegisterVC: UIViewController {
    @IBOutlet weak var createAccountBtn: UIButton! { didSet { createAccountBtn.layer.cornerRadius = 10 } }
    @IBOutlet weak var maleBtn: UIButton! { didSet { maleBtn.layer.cornerRadius = 10 } }
    @IBOutlet weak var femailBtn: UIButton! { didSet { femailBtn.layer.cornerRadius = 10 } }
    @IBOutlet weak var passwordConfirmaton: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    var gender = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func registerUser(username: String, email: String, password: String, passwordConfirm: String, completion: @escaping (Result<RegisterContainer>) -> Void) {
        
        Alamofire.request(AuthRouter.register(name: username, email: email, password: password, passwordConfirm: passwordConfirm, gender: gender)).debugLog().responseData { [weak self] response in
            guard let self = self else { return }
            print("Show Response: ",String.init(data:response.data!, encoding:.utf8)!)
            switch response.result {
            
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(RegisterContainer.self, from: data)
                    print(result.data?.client ?? "no data")
                    
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
                    if let parseResult = json {
                        parseResult["api_token"] as? String
                        parseResult["id"] as? String
                        //                        print("Access Token: \(String(describing: acsessToken!))")
                    }
                    
                    DispatchQueue.main.async {
                        let storyboard = UIStoryboard(name: "Quran", bundle: nil)
                        let home = storyboard.instantiateViewController(identifier: "homeNav") as? UINavigationController
                        home?.modalPresentationStyle = .fullScreen
                        self.present(home!, animated: true, completion: nil)
                    }
                    completion(.success(result))
                } catch  {
                    debugPrint(error.localizedDescription)
                    completion(.failure(error))
                }
            case .failure(let error):
                debugPrint(error.localizedDescription)
                completion(.failure(error))
                
            }
        }
        
    }
    
    @IBAction func maleTapped(_ sender: UIButton) {
        gender = "male"
        sender.isSelected = !sender.isSelected
        sender.tintColor  = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
        DispatchQueue.main.async {
            
            if sender.isSelected {
                sender.backgroundColor = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
            } else {
                sender.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                
            }
        }
    }
    
    @IBAction func femaleTapped(_ sender: UIButton) {
        gender = "female"
        sender.isSelected = !sender.isSelected
        sender.tintColor = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
        if sender.isSelected {
            sender.backgroundColor = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
        } else {
            sender.backgroundColor = .white
        }
    }
    
    @IBAction func createNewAccountPressed(_ sender: UIButton) {
        validate()
        if passwordTF.text?.elementsEqual(passwordConfirmaton.text!) != true {
            print("password dont match")
            return
        }
        
    }
}



extension RegisterVC {
    func validate() {
        do {
            let name = try nameTF.validatedText(validationType: .username)
            let password = try passwordTF.validatedText(validationType: .password)
            let passConfirm = try passwordConfirmaton.validatedText(validationType: .password)
            let email = try emailTF.validatedText(validationType: .email)
            registerUser(username: name, email: email, password: password, passwordConfirm: passConfirm) { (result) in
                if result != nil {
                    print("regiter Done!")
                }
            }
        } catch let error  {
            showAlert(for: (error as! ValidationError).message)
        }
    }
    
    
    func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "متابعة", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
}

