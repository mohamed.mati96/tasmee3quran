//
//  ChangePasswordVC.swift
//  RecitationQuran
//
//  Created by maati on 9/17/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Alamofire

final class ChangePasswordVC: UIViewController {
    @IBOutlet private weak var confirmNewPassBtn: UIButton! {
        didSet {
            confirmNewPassBtn.layer.cornerRadius = 10
            confirmNewPassBtn.clipsToBounds = true
        }
    }
    @IBOutlet private weak var cureentPasswordTF: UITextField!
    @IBOutlet private weak var newPasswordTF: UITextField!
    @IBOutlet private weak var confirmNewPasswordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    

    @IBAction func confirmNewPassTapped(_ sender: UIButton) {
        validate()
    }
    
    func changePassword(oldPass: String, password: String, confirmPass: String) {
        Alamofire.request(AuthRouter.ChangePassword(oldPass: oldPass, password: password, passwordConfirm: confirmPass)).responseData { [weak self ] (response) in
            //            print("Show Response: ",String.init(data:response.data!, encoding:.utf8)!)
            guard let self = self else { return }
            switch response.result {
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(ChangePassword.self, from: data)
                    print(result)
                    self.navigationController?.popViewController(animated: false)
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
        
    }
    
    func validate() {
        do {
            let currentPass = try cureentPasswordTF.validatedText(validationType: ValidatorType.password)
            let password = try newPasswordTF.validatedText(validationType: ValidatorType.password)
            let confirmPass = try confirmNewPasswordTF.validatedText(validationType: ValidatorType.password)
            changePassword(oldPass: currentPass, password: password, confirmPass: confirmPass)
        } catch(let error) {
            showAlert(for: (error as! ValidationError).message)
        }
    }
    
    
    func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "متابعة", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
}
