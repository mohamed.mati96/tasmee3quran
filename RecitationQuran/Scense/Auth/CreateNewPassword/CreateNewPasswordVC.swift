//
//  CreateNewPasswordVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 9/7/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Alamofire

final class CreateNewPasswordVC: UIViewController {

    @IBOutlet private weak var pinCodeTF: UITextField!
    @IBOutlet private weak var newPasswordTF: UITextField!
    @IBOutlet private weak var confairmNewPasswordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
    }
    
    
    @IBAction func goToLogin(_ sender: UIButton) {
        guard let pass = newPasswordTF.text , pass != "" else { return }
        guard let passConfirm = confairmNewPasswordTF.text , passConfirm != ""  else { return }
        guard let pincode = pinCodeTF.text , pincode != "" else { return }
        newPass(pinCode: Int(pincode)!, pass: pass, passConfirm: passConfirm)
    }
    
    func newPass(pinCode: Int, pass: String, passConfirm: String) {
        Alamofire.request(AuthRouter.newPass(pinCode: pinCode, password: pass, passwordConfirm: passConfirm)).responseData { [weak self ] (response) in
            //            print("Show Response: ",String.init(data:response.data!, encoding:.utf8)!)
            guard let self = self else { return }
            switch response.result {
            
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(NewPassword.self, from: data)
                    print(result)
                    print("Show Response: ",String.init(data:response.data!, encoding:.utf8)!)
//                    self.dismiss(animated: true, completion: nil)
////
                    self.navigationController?.popViewController(animated: false)
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    
    
    
    
}
