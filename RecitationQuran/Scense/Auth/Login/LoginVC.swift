//
//  LoginVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/27/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Alamofire

final class LoginVC: UIViewController {
    
    @IBOutlet private weak var emailTF: UITextField!
    @IBOutlet private weak var passwordTF: UITextField!
    @IBOutlet private weak var loginBtn: UIButton! { didSet { loginBtn.layer.cornerRadius = 10 } }
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTF()
    }
    
    @IBAction func forgetPassword(_ sender: UIButton) {
        let  forgetPassword = (self.storyboard?.instantiateViewController(withIdentifier: "ForgetPassword"))! as! ForgetPasswordVC
        self.navigationController?.pushViewController(forgetPassword, animated: true)
    }
    
    @IBAction func goToCreateAccount(_ sender: UIButton) {
        let register = storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterVC
        navigationController?.pushViewController(register, animated: true)
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        validate()
       
    }
    
    func loginUser(email: String, password: String, completion: @escaping (Result<LoginUser>) -> Void) {
        self.view.showActivityIndicator(isUserInteractionEnabled: true)
        Alamofire.request(AuthRouter.login(email: email, password: password)).responseData { [weak self ] (response) in
            //            print("Show Response: ",String.init(data:response.data!, encoding:.utf8)!)
            guard let self = self else { return }
            self.view.hideActivityIndicator()
            switch response.result {
            
            case .success(let data):
                
                do {
                    let result = try JSONDecoder().decode(LoginUser.self, from: data)
                    print(result)
                    switch result.status {
                    case 1:
                        UserDefault.instance.authToken = result.data?.apiToken ?? ""
                        UserDefault.instance.isLoggedIn = true
                        let storyboard = UIStoryboard(name: "Quran", bundle: nil)
                        if #available(iOS 13.0, *) {
                            let home = storyboard.instantiateViewController(identifier: "homeNav") as? UINavigationController
                            home?.modalPresentationStyle = .fullScreen
                            self.present(home!, animated: true, completion: nil)
                            completion(.success(result))
                        } else {
                            // Fallback on earlier versions
                        }
                    case 0:
                        self.showAlert(for: "بيانات الدخول غير صحيحه")
                    default:
                        break
                    }
                    
                } catch {
                    print(error.localizedDescription)
                    completion(.failure(error))
                }
            case .failure(let error):
                debugPrint(error.localizedDescription)
                completion(.failure(error))
            }
        }
    }
}

extension LoginVC {
    
    func configureTF() {
        emailTF.setPlaceHolderTextColor(#colorLiteral(red: 0, green: 0.2274509804, blue: 0.2156862745, alpha: 1))
        passwordTF.setPlaceHolderTextColor(#colorLiteral(red: 0.03722278774, green: 0.2666197717, blue: 0.2860219181, alpha: 1))
        emailTF.textType = .emailAddress
        passwordTF.textType = .password
        emailTF.clear()
    }
  
    func validate() {
        do {
            let email = try emailTF.validatedText(validationType: ValidatorType.email)
            let password = try passwordTF.validatedText(validationType: ValidatorType.password)
            loginUser(email: email, password: password) { (result) in
            }
            
        } catch(let error) {
            showAlert(for: (error as! ValidationError).message)
        }
    }
    
    
    func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "متابعة", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
}
