//
//  ProfileVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 9/3/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Alamofire
import UIKit

final class ProfileVC: UIViewController {
    @IBOutlet private weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 10
            containerView.clipsToBounds = true
            containerView.layer.shadowColor = UIColor.darkGray.cgColor
            containerView.layer.shadowOpacity = 0.5
            containerView.layer.shadowOffset = .zero
            containerView.layer.shadowRadius = 5
        }
    }
    
    @IBOutlet private weak var changePasswordBtn: UIButton! {
        didSet {
            changePasswordBtn.layer.cornerRadius = 10
            changePasswordBtn.clipsToBounds = true
        }
    }
    @IBOutlet private weak var genderLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    
    private var userProfileModel: Profile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "الملف الشخصي"
        featchData()
    }
    
    fileprivate func featchData() {
        view.showActivityIndicator(isUserInteractionEnabled: true)
        Alamofire.request(AuthRouter.UserProfile).debugLog().responseData { [weak self] response in
            guard let self = self else { return }
            print("Show Response: ", String(data: response.data!, encoding: .utf8)!)
            self.view.hideActivityIndicator()
            switch response.result {
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(UserProfile.self, from: data)
                    self.userProfileModel = result.data
                    self.configure()
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    fileprivate func configure() {
        guard let item = userProfileModel else { return }
        nameLabel?.text = item.name
        emailLabel?.text = item.email
        genderLabel?.text = item.gender
    }
    
    @IBAction fileprivate func goToChangePasswordTapped(_ sender: UIButton) {
        let changePassVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "changePassword") as? ChangePasswordVC
        navigationController?.pushViewController(changePassVC!, animated: true)
    }
}
