//
//  TeacherProfileVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/31/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit

class TeacherProfileView: UIView {
    
  
    @IBOutlet var twitterButtonCollection: [UIButton]!
    @IBOutlet var instaButtonCollection: [UIButton]!
    
    @IBOutlet private weak var profileImageView: UIImageView!
    @IBOutlet private weak var teacherNameLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    
    @IBOutlet private weak var contactWithTeacherBtn: UIButton! { didSet { contactWithTeacherBtn.layer.cornerRadius = 10 } }
    
    @IBOutlet private weak var backgroundView: UIView! {
        didSet {
            backgroundView.layer.shadowColor = UIColor.darkGray.cgColor
            backgroundView.layer.shadowOpacity = 0.5
            backgroundView.layer.shadowOffset = .zero
            backgroundView.layer.shadowRadius = 5
            backgroundView.layer.cornerRadius = 5
        }
    }
//    weak var delegate: TeacherContactDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBAction func gotoTwitter(_ sender: UIButton) {
    }
    
    @IBAction func goToInsta(_ sender: UIButton) {
        
    }

    func configure(item: TeacherProfile) {
        teacherNameLabel.text = item.name
        emailLabel.text       = item.email
        phoneLabel.text       = item.phone
        profileImageView.kf.indicatorType = .activity
        profileImageView.kf.setImage(with: URL(string: item.imageFullPath! ))
        
    
    }
    
    
}
