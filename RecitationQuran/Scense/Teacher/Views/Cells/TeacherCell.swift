//
//  TeacherCell.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/31/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Kingfisher

class TeacherCell: UICollectionViewCell {
    @IBOutlet weak var teacherImageView: UIImageView! {
        didSet{
            teacherImageView.layer.cornerRadius = teacherImageView.frame.size.width / 2
            teacherImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var teacherNameLabel: UILabel!
    @IBOutlet weak var backgroundUIView: UIVisualEffectView! {
        didSet {
            backgroundUIView.layer.shadowColor = UIColor.black.cgColor
            backgroundUIView.layer.shadowOpacity = 1
            backgroundUIView.layer.shadowOffset = .zero
            backgroundUIView.layer.shadowRadius = 10
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(item: Teachers) {
        teacherNameLabel.text = item.name
        teacherImageView.kf.indicatorType = .activity
        teacherImageView.kf.setImage(with: URL(string: item.imageFullPath ?? "no image" ))
    }
    
}
