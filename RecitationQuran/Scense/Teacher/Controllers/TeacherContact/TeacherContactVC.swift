//
//  TeacherContactVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 9/1/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

enum AGAudioRecorderState {
    case Pause
    case Play
    case Finish
    case Failed(String)
    case Recording
    case Ready
    case error(Error)
}

protocol AGAudioRecorderDelegate {
 
    func agAudioRecorder(_ recorder: TeacherContactVC, withStates state: AGAudioRecorderState)
    func agAudioRecorder(_ recorder: TeacherContactVC, currentTime timeInterval: TimeInterval, formattedString: String)
}


class TeacherContactVC: UIViewController {
    private var isAudioRecordingGranted: Bool = false
      private var filename: String = ""
      
      private var recorderState: AGAudioRecorderState = .Ready {
          willSet{
              delegate?.agAudioRecorder(self, withStates: newValue)
          }
      }
    var delegate: AGAudioRecorderDelegate?

    @IBOutlet private weak var textMessageView: UITextView! {
        didSet {
            textMessageView.layer.borderWidth = 1
            textMessageView.layer.borderColor = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
            textMessageView.layer.cornerRadius = 5
        }
    }
    @IBOutlet private weak var sendBtn: UIButton! { didSet { sendBtn.layer.cornerRadius = 10 } }
    @IBOutlet private weak var stopButton: UIButton!
    @IBOutlet private weak var recordButton: UIButton!
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var timerLabel: UILabel!
    
    var userId: Int?
    var recordSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "تواصل مع المحفظ"
        recorderState = .Ready
        check_record_permission()
    }
    
    
  
    private func check_record_permission() {
            
        switch AVAudioSession.sharedInstance().recordPermission {
            case .granted:
                isAudioRecordingGranted = true
                break
            case .denied:
                isAudioRecordingGranted = false
                break
            case .undetermined:
                AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                    }
                })
                break
        @unknown default:
            fatalError()
        }
        }
        
        private func documentsDirectory() -> URL {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let documentsDirectory = paths[0]
            return documentsDirectory
        }
        
        func fileUrl() -> URL {
            let filename = "audio.m4a"
            let filePath = documentsDirectory().appendingPathComponent(filename)
            return filePath
        }
        
        func changeFile(withFileName filename: String) {
            self.filename = filename
            
            if audioPlayer != nil {
                doPause()
            }
            
            if audioRecorder != nil {
                doStopRecording()
                setupRecorder()
            }
        }
    
    private func setupRecorder() {
        
        if isAudioRecordingGranted {
            let session = AVAudioSession.sharedInstance()
            do {
                try session.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 12000,
                    AVNumberOfChannelsKey: 1,
                    AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
                ]
                
                audioRecorder = try AVAudioRecorder(url: fileUrl(), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()
                self.recorderState = .Ready
            }
            catch let error {
                recorderState = .error(error)
            }
        }
        else {
            recorderState = .Failed("Don't have access to use your microphone.")
        }
    }
    private func finishAudioRecording(success: Bool) {
            if success {
                audioRecorder?.stop()
             
                recorderState = .Finish
            }
            else {
                recorderState = .Failed("Recording failed.")
            }
        }
        
        private func preparePlay() {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: fileUrl())
                audioPlayer.delegate = self
                audioPlayer.prepareToPlay()
                recorderState = .Ready
            }
            catch {
                recorderState = .error(error)
                debugPrint(error)
            }
        }
        
        func doRecord() {
            if audioRecorder == nil {
                setupRecorder()
                
            }
            
            if audioRecorder.isRecording {
                doStopRecording()
                pauseTimer()

            }
            
            else {
                audioRecorder.record()
                startTimer()
                recorderState = .Recording
            }
        }
        
        func doStopRecording() {
            
            guard audioRecorder != nil else {
                return
            }
            
            audioRecorder.stop()
            let audioSession = AVAudioSession.sharedInstance()
            do {
                try audioSession.setActive(false)
                finishAudioRecording(success: true)
                resetTimer()
            } catch {
                recorderState = .error(error)
            }
        }
        
        func doPlay() {
            
            if audioPlayer == nil {
                self.preparePlay()
                startTimer()
            }
            
            if audioRecorder != nil, audioRecorder.isRecording {
                self.doStopRecording()
            }
            
            if audioPlayer.isPlaying {
                doPause()
            }
            else{
                if FileManager.default.fileExists(atPath: fileUrl().path) {
                    preparePlay()
                    audioPlayer.play()
                    recorderState = .Play
                }
                else {
                    recorderState = .Failed("Audio file is missing.")
                }
            }
        }
        
        func doPause() {
            
            guard audioPlayer != nil else {
                return
            }
            
            if audioRecorder != nil, audioRecorder.isRecording {
                self.doStopRecording()
            }
            
            if (audioPlayer.isPlaying){
                audioPlayer.pause()
            }
            recorderState = .Pause
        }
    // MARK: - Helper methods
    
   
        
    @IBAction func playButtonTapped(_ sender: UIButton) {
       doPlay()

    }
    
    
    @IBAction func recordButtonTapped(_ sender: UIButton) {
        doRecord()
    }
    
    @IBAction func stopButtonTapped(_ sender: UIButton) {
        doStopRecording()
    }
    
    func sendAudio(audioPath: URL) {
      
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(audioPath, withName: "audio")
            multipartFormData.append((UserDefault.instance.authToken).data(using: .utf8)!, withName: "api_token")
            multipartFormData.append("\(self.userId ?? 0)".data(using: .utf8, allowLossyConversion: false)!, withName: "user_id")
             },
           usingThreshold:UInt64.init(),
           to:"https://www.yourlandsa.com/RecitationQuran/api/v1/contact-audio",
           method:.post,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    debugPrint(response)
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })

    }

    func featchData(userId: Int)  {
        view.showActivityIndicator(isUserInteractionEnabled: true)
        Alamofire.request(ContactRouter.Message(message: textMessageView.text, token: UserDefault.instance.authToken, userId: userId)).debugLog().responseData { [weak self] response in
            guard let self = self else { return }
            print("Show Response: ",String.init(data:response.data!, encoding:.utf8)!)
            self.view.hideActivityIndicator()
            switch response.result {
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(SendData.self, from: data)
                    print(result)
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func sendData(_ sender: UIButton) {
        guard let audioUrl: URL? = audioRecorder.url else { return }
        guard let url = audioUrl else {
            print("URL Is Nil") // TODO: Show Alert to user not record any thing
            return
        }
        sendAudio(audioPath: url)
        if let id = userId {
            featchData(userId: id)

        }
    }
    
    
    
  
//     MARK: - Timer

    private var timer: Timer?
    private var elapsedTimeInSecond: Int = 0

    func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            self.elapsedTimeInSecond += 1
            self.updateTimeLabel()
        })

    }

    func pauseTimer() {
        timer?.invalidate()
    }

    func resetTimer() {
        timer?.invalidate()
        elapsedTimeInSecond = 0
        updateTimeLabel()
    }

    func updateTimeLabel() {

        let seconds = elapsedTimeInSecond % 60
        let minutes = (elapsedTimeInSecond / 60) % 60

        timerLabel.text = String(format: "%02d:%02d", minutes, seconds)
    }
    
}

extension TeacherContactVC: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            let alertMessage = UIAlertController(title: "انتهي التسجيل", message: "تم تسجيل الصوت بنجاح", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
            present(alertMessage, animated: true, completion: nil)
        }
     
    }
}

extension TeacherContactVC: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        let alertMessage = UIAlertController(title: "انتهي التشغيل", message: "انتهي تشغيل الصوت ", preferredStyle: .alert)
        alertMessage.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
        present(alertMessage, animated: true, completion: nil)
    }
}
