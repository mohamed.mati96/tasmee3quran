//
//  TeacherProfileVC.swift
//  RecitationQuran
//
//  Created by maati on 9/11/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Alamofire
import SafariServices

protocol TeacherContactDelegate: class {
    func navigateToContact(sender: TeacherContactVC)
}

class TeacherProfileVC: UIViewController {

    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var instaButton: UIButton!
    private lazy var teacherProfileView: TeacherProfileView = {
        let teacherProfileView = Bundle.loadView(fromNib: "TeacherProfileView", withType: TeacherProfileView.self)
        return teacherProfileView
    }()
   private var teacherProfileModel: TeacherProfile? {
        didSet {
            guard let item = teacherProfileModel else { return }
            teacherProfileView.configure(item: item)
        }
    }
   private weak var teacherProfile: TeacherProfileView?
    var userId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuraNav()
       
        if let id = userId {
            featchData(userId:  id) }
        view.addSubview(teacherProfileView)
    }
    
   
    @IBAction func twitterTapped(_ sender: UIButton) {
        guard let twetter = teacherProfileModel?.twitterLink else { return }
        if let url = URL(string: twetter) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func instaTapped(_ sender: UIButton) {
        guard let insta = teacherProfileModel?.instagramLink else { return }
        if let url = URL(string: insta) {
            UIApplication.shared.open(url)
        }
    }
    
    func featchData(userId: Int)  {
        view.showActivityIndicator(isUserInteractionEnabled: true)
        Alamofire.request(TeacherRouter.teacherProfile(userId: userId)).debugLog().responseData { [weak self] response in
            guard let self = self else { return }
            print("Show Response: ",String.init(data:response.data!, encoding:.utf8)!)
            self.view.hideActivityIndicator()
            switch response.result {
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(TeacherProfileResonse.self, from: data)
                    self.teacherProfileModel = result.data
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    

    @IBAction func goToSendAudioAndMessage(sender: UIButton) {
        let vc = TeacherContactVC(nibName: "TeacherContactVC", bundle: nil)
        vc.userId = userId
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func configuraNav()  {
        navigationItem.title = "الملف الشخصي"
        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)
        let textAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8440434337, green: 0.6796635985, blue: 0.4624471068, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
  
    
}
