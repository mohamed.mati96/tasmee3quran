//
//  TeacherVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/31/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import Alamofire
import SideMenu


final class TeacherVC: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate   = self
        }
    }
    
   private var teacher =  [Teachers]()  { didSet { filteredTeacher = teacher  } }
   private var filteredTeacher  = [Teachers]() { didSet { collectionView.reloadData() }}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.navigationItem.title = "قائمة المحفظين"
        featchData()
        collectionView.register(UINib(nibName: "TeacherCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        let width = (view.frame.size.width - 0) / 2
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width)
    }
    
    func featchData()  {
        view.showActivityIndicator(isUserInteractionEnabled: true)
        Alamofire.request(TeacherRouter.teacherList).debugLog().responseData { [weak self] response in
            guard let self = self else { return }
            self.view.hideActivityIndicator()
            switch response.result {
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(TeachersListRepsonse.self, from: data)
                    debugPrint(result)
                    self.teacher = result.data ?? []
                } catch {
                    debugPrint(error.localizedDescription)
                }
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
}

extension TeacherVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredTeacher.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TeacherCell
        cell.configure(item: filteredTeacher[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let teacherProfileVC = TeacherProfileVC()
        teacherProfileVC.userId = filteredTeacher[indexPath.row].id
        navigationController?.pushViewController(teacherProfileVC, animated: true)
        
    }
    
}

extension TeacherVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredTeacher = searchText.isEmpty ?  teacher : teacher.filter { ($0.name?.lowercased().contains(searchText.lowercased()))!}
    }
}

