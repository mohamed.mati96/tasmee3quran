//
//  SideMenuVC.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/29/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import SideMenu

protocol MenuControllerDelegate {
    func didSelectMenuItem(named: SideMenuItem)
}

enum SideMenuItem: String, CaseIterable {
    case profile = "حسابي"
    case home = "الرئيسية"
    case shareApp = "مشاركة التطبيق"
    case rateApp = "تقيم التطبيق"
    case logout = "تسجيل الخروج"
}

class SideMenuVC: UITableViewController {
    public var delegate: MenuControllerDelegate?
    private let menuItems: [SideMenuItem]
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        navigationController as? UISideMenuNavigationController 
    }
    init(with menuItems: [SideMenuItem]) {
        self.menuItems = menuItems
        super.init(nibName: nil, bundle: nil)
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "menuCell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = #colorLiteral(red: 0.9607843137, green: 1, blue: 0.9921568627, alpha: 1)
        tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
        cell.titleMenuLabel.text = menuItems[indexPath.row].rawValue
        cell.logoMenuImageView.image = UIImage(named: "menu_icon_" + String(indexPath.row + 1))
        cell.textLabel?.textAlignment = .right
        cell.backgroundColor = .clear
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedItem = menuItems[indexPath.row]
        delegate?.didSelectMenuItem(named: selectedItem)
    }
    
}


