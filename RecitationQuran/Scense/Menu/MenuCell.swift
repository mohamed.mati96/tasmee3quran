//
//  MenuCell.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 9/2/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var logoMenuImageView: UIImageView!
    @IBOutlet weak var titleMenuLabel: UILabel! {
        didSet {
            titleMenuLabel.font = UIFont(name: "Tajawal-Medium", size: 16)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
