//
//  SourahRouter.swift
//  RecitationQuran
//
//  Created by maati on 9/10/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation
import Alamofire

enum SourahRouter: URLRequestConvertible {
    
    case sourahList
    case sourahDetails
    var method: HTTPMethod {
        switch self {
        case .sourahList:
            return .get
        case .sourahDetails:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .sourahDetails:
            return nil
        case .sourahList:
            return nil
        default:
            return nil
        }
        
    }
    var url: URL {
        var baseURL: String  = "http://api.alquran.cloud/v1/"
        switch self {
        case .sourahList:
            baseURL = baseURL +  "surah"
        case .sourahDetails:
            baseURL =  "http://api.alquran.cloud/v1/surah/"
        }
        return URL(string: baseURL)!
    }
    var encoding: ParameterEncoding {
        switch self {
        case .sourahList:
            return URLEncoding.queryString
        case .sourahDetails:
            return URLEncoding.default
        }
    }
    
}
