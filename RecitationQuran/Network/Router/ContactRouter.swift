//
//  ContactRouter.swift
//  RecitationQuran
//
//  Created by maati on 9/14/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation
import Alamofire

enum ContactRouter: URLRequestConvertible {
    
    case Message(message: String, token: String, userId: Int)
    
    var method: HTTPMethod {
        switch self {
        case .Message:
            return .post
        default:
            return .get
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .Message(let message, let token, let userId):
            return ["message": message,"api_token": token, "user_id": userId]
        default:
            return nil
        }
    }
    
    var url: URL {
        let endPoint: String
        switch self {
        case .Message:
            endPoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/contact-messages"
        }
        return URL(string: endPoint)!
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .Message:
            return URLEncoding.httpBody
        }
    }
    

    
}
