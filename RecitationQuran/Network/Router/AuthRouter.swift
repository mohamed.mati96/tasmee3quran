//
//  AuthRouter.swift
//  CareAndCure
//
//  Created by Mohamed M3aty on 5/18/20.
//  Copyright © 2020 Mohamed M3aty. All rights reserved.
//

import Foundation
import Alamofire

enum AuthRouter: URLRequestConvertible {
    
    case register(name: String, email: String, password: String, passwordConfirm: String, gender: String)
    case login(email: String, password: String)
    case forgetPass(email: String)
    case newPass(pinCode: Int, password: String, passwordConfirm: String)
    case UserProfile
    case ChangePassword(oldPass: String, password: String, passwordConfirm: String)
    var method: HTTPMethod {
        switch self {
        case .register:
            return .post
        case .login:
            return .post
        case .forgetPass:
            return .post
        case .newPass:
            return .post
        case .UserProfile:
            return .post
        case .ChangePassword:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .register(let name, let email, let password, let passwordConfirm, let gender):
            return ["name": name, "email": email, "password": password, "password_confirmation": passwordConfirm, "gender": gender]
        case .login(let email, let password):
            return ["email": email, "password": password]
        case .forgetPass(let email):
            return ["email": email]
        case .newPass(let pinCode, let password, let passwordConfimation):
            return ["pin_code": pinCode,"password": password, "password_confirmation": passwordConfimation]
        case .UserProfile:
            return ["api_token": UserDefault.instance.authToken]
        case .ChangePassword(let oldPass, let password, let newPass):
            return ["api_token": UserDefault.instance.authToken,"old_password": oldPass, "password": password, "password_confirmation": newPass]
        default:
            return nil
        }
        
    }
    
    var url: URL {
        let endpoint: String
        switch self {
        case .register:
            endpoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/register"
        case .login:
            endpoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/login"
        case .forgetPass:
            endpoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/reset-password"
        case .newPass:
            endpoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/new-password"
        case .UserProfile:
            endpoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/profile"
        case .ChangePassword:
            endpoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/change-password"
        }
        return URL(string: endpoint)!
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .register:
            return URLEncoding.httpBody
        case .login:
            return URLEncoding.httpBody
        case .forgetPass:
            return URLEncoding.httpBody
        case .newPass:
            return URLEncoding.httpBody
      
        case .UserProfile:
            return URLEncoding.httpBody
        case .ChangePassword:
            return URLEncoding.httpBody
        }
    }
    
    
}


