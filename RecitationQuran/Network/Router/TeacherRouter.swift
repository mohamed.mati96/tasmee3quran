//
//  TeacherRouter.swift
//  RecitationQuran
//
//  Created by maati on 9/9/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import Foundation
import Alamofire

enum TeacherRouter: URLRequestConvertible {
    
    case teacherList
    case teacherProfile(userId: Int)
    var method: HTTPMethod {
        switch self {
        case .teacherList:
            return .get
        case .teacherProfile:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .teacherProfile(let user_id):
            return ["user_id": user_id, "api_token": UserDefault.instance.authToken]
        case .teacherList:
            return ["api_token": UserDefault.instance.authToken]
        default:
            return nil
        }
        
    }
    var url: URL {
        var endPoint: String?
        switch self {
        case .teacherList:
            endPoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/list-of-admins"
        case .teacherProfile:
            endPoint = "https://www.yourlandsa.com/RecitationQuran/api/v1/admin-profile"
        }
        
        return URL(string: endPoint!)!
    }
    var encoding: ParameterEncoding {
        switch self {
        case .teacherList:
            return URLEncoding.queryString
        case .teacherProfile:
            return URLEncoding.default
        }
    }
    
}
