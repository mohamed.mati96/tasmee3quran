//
//  AuthRouter.swift
//  CareAndCure
//
//  Created by Mohamed M3aty on 5/18/20.
//  Copyright © 2020 Mohamed M3aty. All rights reserved.
//

import Foundation
import Alamofire

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint("=======================================")
        debugPrint(self)
        debugPrint("=======================================")
        #endif
        return self
    }
}
