//
//  AppDelegate.swift
//  RecitationQuran
//
//  Created by Mohamed M3aty on 8/27/20.
//  Copyright © 2020 Open Shop. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        if UserDefault.instance.isLoggedIn {
            print("is loggedIn Value: ",UserDefault.instance.isLoggedIn)
            let nav = UIStoryboard(name: "Quran", bundle: nil).instantiateViewController(identifier: "homeNav") as? UINavigationController
            window?.rootViewController = nav
            window?.makeKeyAndVisible()

        } else {
            let mainlogin = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainLogin")
            window?.rootViewController = mainlogin
            window?.makeKeyAndVisible()
        }
        attemptToRegisterForNotifications(application: application)
        return true
    }
    
    
    func attemptToRegisterForNotifications(application: UIApplication) {
        
        Messaging.messaging().delegate = self
        
        UNUserNotificationCenter.current().delegate = self
        
        let options: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: options) { (authorized, error) in
            if authorized {
                print("DEBUG: SUCCESSFULLY REGISTERED FOR NOTIFICATIONS")
            }
        }
        
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("DEBUG: Registered for notifications with device token: ", deviceToken)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("DEBUG: Registered with FCM Token: ", fcmToken)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(.alert)
    }
    
}

